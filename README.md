# DILEMME DU PRISONNIER :video_game:

## Pour commencer :clipboard:

Le doctorant a besoin d’accumuler des données expérimentales sur le dilemme du prisonnier. <br>

Le dilemme du prisonnier caractérise en théorie des jeux une situation où deux joueurs auraient intérêt à coopérer mais où, en l'absence de communication entre les deux joueurs, chacun choisira de trahir l'autre si le jeu n'est joué qu'une fois.<br>

La raison est que si l'un collabore et que l'autre trahit, le collaborateur est fortement pénalisé. Pourtant, si les deux joueurs trahissent, le résultat leur est moins favorable que si les deux avaient choisi de collaborer.

Le doctorant a besoin que des volontaires jouent l’un contre l’autre un nombre de fois à définir , sans jamais savoir qui sont leurs adversaires. <br>
On définira une partie comme étant un certain nombre de rounds. Un round est défini comme une confrontation trahison-collaboration entre les deux volontaires.


### Pré-requis :warning: 

Un éxecutable du client et du serveur est fourni avec ce projet. <br> 

Le code source du projet est également fourni.

### Installation

Si on souhaite lancé le projet avec le code source il faut rajouter dans Clion les fichiers glade dans le compilateur. <br> 

Ensuite il suffit de compiler le projet avec les commandes suivantes: <br>


## Fonctionnement de l'application :notebook_with_decorative_cover:

Il faut lancer dans un premier temps le serveur pour que des personnes puissent se connecter. <br> 

Ensuite il faut renseigner dans le fichier de configuration (Setting.cfg) l'addresse IP du serveur et le port qui sera utilisé pour se connecté. 

Une fois le fichier configuré le client peut être lancé et une fois le client lancé il suffit d'attendre qu'un autre joueur se connecte pour que la partie se lance. <br>

Une fois la partie lancé le joueur a le choix entre trahir ou collaboré et une fois que les deux joueurs ont fait leurs choix, les résultats apparaissent.<br>
Au bout d'un nombre de round défini par les joueurs, la partie est terminé.

## Fabriqué avec

* [CLION](https://www.jetbrains.com/fr-fr/clion/) - IDE
* [Glade](https://glade.gnome.org/) - User interface design



## Versions :page_facing_up:

**Dernière version :** 1.0
Liste des versions : [Cliquer pour afficher](https://gitlab.com/Spinarial/c-prisoners-dilema-client)

## Auteurs :japanese_goblin: 

* **Eloi Desbrosses** _alias_ [@Spinarial](https://gitlab.com/Spinarial)
* **Franck Desfrancais** _alias_ [@Franckdsf](https://gitlab.com/Franckdsf)
* **Lucas Chanaux** _alias_ [@luc614fr](https://gitlab.com/luc614fr)
* **Lois Chabrier** _alias_ [@Lois07](https://gitlab.com/Lois07)
