//
// Created by serveur on 11/12/2019.
//

/**
 * @headerfile
 */


#ifndef C_PRISONERS_DILEMA_CLIENT_VIEW_H
#define C_PRISONERS_DILEMA_CLIENT_VIEW_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk-3.0/gtk/gtk.h>
#include <gtk-3.0/gtk/gtkwidget.h>
#include <time.h>

#include "../struct/socket.h"
#include "../controller.h"
#include "../struct/settings.h"

GtkBuilder *builder = NULL;

//les windows
GtkWidget *app_win;
GtkWidget *restart_win;
//les views
GtkGrid *game_win;
GtkGrid *recap_win;
GtkGrid *start_win;
GtkGrid *final_win;

//ID des timers
int game_timeout_timer = -1;
int start_timeout_timer = -1;
int recap_timeout_timer = -1;


//fonctions
void joueurTrahir(GtkButton *button, gpointer user_data);

void joueurCollab(GtkButton *button, gpointer user_data);

void recapRound_view_init(int result);

void finalRecap_view_init();

void game_timer_handler();

void game_view_init();

void start_game_init(bool start_timer);
void start_timer_handler();

gboolean closeApp(GtkWidget *widget, GdkEvent  *event, gpointer   user_data);

void leave_info_view_init();

void quit_Game(GtkButton *button, gpointer);
void restart_Game(GtkButton *button, gpointer);

void init_view(int, char**);

#endif //C_PRISONERS_DILEMA_CLIENT_VIEW_H
