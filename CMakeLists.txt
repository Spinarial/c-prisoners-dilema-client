
cmake_minimum_required(VERSION 3.15)
project(c_prisoners_dilema_client C)

set(CMAKE_C_STANDARD 11)

find_package(PkgConfig REQUIRED)
pkg_check_modules(GTK3 REQUIRED gtk+-3.0)

include_directories(${GTK3_INCLUDE_DIRS})
link_directories(${GTK3_LIBRARY_DIRS})

add_definitions(${GTK3_CFLAGS_OTHER})

add_executable(c_prisoners_dilema_client
        main.c struct/settings.c struct/settings.h struct/socket.c struct/socket.h controller.h main.h view/view.h)

target_link_libraries( c_prisoners_dilema_client pthread )
target_link_libraries(c_prisoners_dilema_client ${GTK3_LIBRARIES})


set(CMAKE_C_FLAGS "-rdynamic")