//
// Created by serveur on 08/11/2019.
//

/**
 * @file
 */


#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "settings.h"


/**
 * @author Lucas, Lois, Franck
 * @param setting
 * @param filename
 * @return false si le fichier n'existe pas
 * @brief fonction qui lit le fichier et stock son contenu dans sa structure dédiée
 */
bool settings_initialize(Settings *setting, char *filename){

    //Setup filename
    setting->filename = filename;

    //Setup file pointer
    setting->settings_file = fopen(filename, "r");

    //check if file exists
    if (setting->settings_file == NULL){
        printf("Le fichier %s n'existe pas", filename);
        printf("\nEchec de connexion au serveur");
        return false;
    }

    //read line by line
    char* line = malloc(SETTINGS_LINE_SIZE);
    int i = 0;

    while (fgets(line, SETTINGS_LINE_SIZE, setting->settings_file) != NULL)  {

        if(i == SETTINGS_MAX_SETTINGS)
            break;

        // Returns first token
        char *value = strtok(line, "=");

        setting->settings_label[i] = malloc(sizeof(value));

        strncpy (setting->settings_label[i] , value, sizeof(setting->settings_value[i]) + 15);

        value = strtok(NULL, "=");
        setting->settings_value[i] = malloc(sizeof(value));

        strncpy (setting->settings_value[i] , value, sizeof(setting->settings_value[i]) + 15);

        i++;
    }

    return true;
}

/**
 * Eloi, Franck, Lois
 * @param settings
 * @param option
 * @return la valeur du label choisit ou error
 * @brief fonction qui permet de récuperer la valeur stockée dans les settings
 * à partir de son label
 */
char* settings_getOption(Settings* settings, char* option){

    //read in settings
    //if option is in it
    for(int i = 0; i <= SETTINGS_MAX_SETTINGS; i++){
        if(settings->settings_label[i] != NULL && strcmp(settings->settings_label[i], option) == 0){
            return settings->settings_value[i];
        }
    }
    printf("option %s not found\n", option);
    return "ERROR";
}